require 'pp'

a = [1,2,3,4,5]
pp a
a2 = Array.new(5, "abc")
pp a2

a.each do |x|
  print "#{2*x} "
end

[5,4,3,2,1].each_with_index do |x,idx|
  print "#{idx}:#{x}, "
end
puts ''

pp a + [6,7,8]

b = a.map {|x| 2*x }
pp b
b.map! {|x| 3*x }
pp b

c = a.find_all {|x| x > 3 }
pp c

d = [4,1,3,2,5]
pp d.sort!

e = [4,1,2,3,5]
e.sort_by! {|x| (3-x).abs }
pp e

f = [5,1,4,2,3]
pp f.index(2)

g = [2,4,6,8]
pp g.all? {|x| x % 2 == 0 }
h = [2,4,6,9]
pp h.all? {|x| x % 2 == 0 }

i = [1,3,5,7,9]
pp i.inject(0) {|sum,x| sum + x }

j = [5,1,1,2,3,5,5]
pp j.uniq!

k = [4,5,1,2,3,4,5]
k.uniq! {|x| x % 3 }
pp k
