#include <iostream>
#include <array>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <set>
#include <map>
#include <list>
#include <random>

template <class T>
std::string Inspect( const std::vector<T>& v ) {
  std::ostringstream oss;
  oss << "[";
  for( size_t i=0; i < v.size(); i++) {
    oss << v[i];
    if( i != v.size() - 1 ) { oss << ", "; }
    else { oss << "]" << std::endl; }
  }
  return oss.str();
}

template <class T>
void P( const std::vector<T>& v, std::ostream& os = std::cout ) {
  os << Inspect(v);
}

void Initialize() {
  std::vector<long> a = {1,2,3,4,5}; // initializer list syntax
  P(a);
  std::vector<std::string> a2(5, "abc");
  P(a2);
}

void Each() {
  std::vector<long> a = {1,2,3,4,5};
  for( const auto& x : a ) {
    std::cout << 2 * x << ", ";
  }
  std::cout << std::endl;
}

void EachWithIndex() {
  std::vector<long> a = {5,4,3,2,1};
  for( size_t i=0; i < a.size(); i++) {
    std::cout << i << ":" << a[i] << ", ";
  }
  std::cout << std::endl;
}

void Concat() {
  std::vector<long> v1 = {1,2,3};
  std::vector<long> v2 = {4,5};
  v1.insert( v1.end(), v2.begin(), v2.end() );
  P( v1 );
  P( v2 );
}

void Push() {
  std::vector<long> v = {1,2};
  v.push_back(3);
  P(v);
}

void Map() {
  std::vector<long> a = {1,2,3};
  auto twice = [](long x)->long { return 2*x; };

  for( auto& x : a ) { twice(x); }
  P( a );
}

void Find() {
  std::vector<long> v = {7,8,9,10,11};
  auto f = [](long x)->bool { return (x%5==0); };

  auto found = std::find_if( v.begin(), v.end(), f );
  if( found == v.end() ) {
    std::cout << "Not found" << std::endl;
  }
  else {
    std::cout << "Found: " << *found << std::endl;
  }
}

void FindForSorted() {
  std::vector<long> v = {7,8,9,10,11};

  auto found = std::lower_bound( v.begin(), v.end(), 10 );
  if( found == v.end() ) {
    std::cout << "Not found" << std::endl;
  }
  else {
    std::cout << "Found: " << *found << std::endl;
  }
}

void FindAll() {
  std::vector<long> a = {1,2,3,4,5};
  std::vector<long> result;

  auto condition = [](long x)->bool { return (x>3); };
  for( const auto& x : a ) {
    if( condition(x) ) { result.push_back(x); }
  }
  P(result);
}

void Sort() {
  std::vector<long> v = {4,1,3,2,5};
  std::sort( v.begin(), v.end() );
  P(v);
}

void SortBy() {
  std::vector<long> v = {4,1,2,3,5};
  auto f = [](long x)->long { return std::abs(3-x); };

  std::sort(
      v.begin(),
      v.end(),
      [&](const long& lhs, const long& rhs)->bool {
        return( f(lhs) < f(rhs) );
        }
      );
  P(v);
}

void Index() {
  std::vector<long> v = {5,1,4,2,3};
  auto f = [](long x)->bool { return (x == 2); };

  auto found = std::find_if(v.begin(), v.end(), f );
  std::cout << std::distance(v.begin(), found) << std::endl;

  // another implementation
  /*
  size_t idx = 0;
  for( idx=0; idx < v.size(); idx++) {
    if( f(v[idx]) ) { break; }
  }
  std::cout << idx << std::endl;
  */
}

void All() {
  std::vector<long> v = {2,4,6,8};
  bool b1 = std::all_of( v.begin(), v.end(), [](long x) {
    return x%2==0;
  });
  std::cout << (b1?"true":"false") << std::endl;

  bool b2 = std::all_of( v.begin(), v.end(), [](long x) {
    return x < 8;
  });
  std::cout << (b2?"true":"false") << std::endl;
}

void Sum() {
  std::vector<long> v = {1,3,5,7,9};
  long sum = std::accumulate(v.begin(), v.end(), 0);  // #include <numeric>
  std::cout << sum << std::endl;
}

void Inject() {
  std::vector<long> v = {1,2,3,4,5};
  long prod = std::accumulate(v.begin(), v.end(), 1, [](long acc, long x) {
    return acc*x;
  });
  std::cout << "prod: " << prod << std::endl;

  std::string str = std::accumulate(v.begin(), v.end(), std::string(), [](const std::string& acc, long x)->std::string {
    return acc + ((x%2==0)?"e":"o");
  });
  std::cout << "str:  " << str << std::endl;
}

void Uniq() {
  std::vector<long> v = {5,1,1,2,3,5,5};
  
  std::set<long> tmpset;
  auto sorted = v.begin();
  for( auto it = v.begin(); it != v.end(); ++it ) {
    if( tmpset.insert(*it).second ) {
      *sorted = *it;
      sorted++;
    }
  }
  v.erase( sorted, v.end() );
  
  P(v);
}

void UniqBy() {
  std::vector<long> v = {4,5,1,2,3,4,5};
  auto f = [](long x)->long { return x % 3; };

  std::set<long> tmpset;
  auto sorted = v.begin();
  for( auto it = v.begin(); it != v.end(); ++it ) {
    if( tmpset.insert( f(*it) ).second ) {
      *sorted = *it;
      ++sorted;
    }
  }
  v.erase( sorted, v.end() );

  P(v);
}

void UniqForSorted() {
  std::vector<long> v = {1,1,2,3,4,4,4};
  v.erase( std::unique(v.begin(), v.end()), v.end() );
  P(v);
}

void Join() {
  std::vector<long> v = {1,5,3,2};
  auto io = std::ostringstream();
  std::string separator = ":";

  for(auto it = v.begin(); it != v.end(); ) {
    io << *it;
    if( (++it) != v.end() ) { io << separator; }
  }

  std::cout << io.str() << std::endl;
}

void Reverse() {
  std::vector<long> v = {1,2,3};
  std::reverse( v.begin(), v.end() );
  
  P(v);
}

void GroupBy() {
  std::vector<std::string> v = {"cat","bat","bear","camel","alpaca"};
  auto f = [](const std::string& s)-> char { return s.c_str()[0]; };

  std::map<char, std::vector<std::string> > result;
  for( const auto& x : v ) {
    result[ f(x) ].push_back(x);
  }
  
  for( auto kv : result ) {
    std::cout << kv.first << " : " << Inspect(kv.second);
  }
}

void Shuffle() {
  std::vector<long> v = {1,2,3,4,5,6,7};

  std::mt19937 rnd(1234);
  std::shuffle(v.begin(), v.end(), rnd);

  P(v);
}

int main( int, char** ) {

  std::cout << "Initialize:" << std::endl;
  Initialize();
  std::cout << "Each:" << std::endl;
  Each();
  std::cout << "EachWithIndex:" << std::endl;
  EachWithIndex();
  std::cout << "Concat:" << std::endl;
  Concat();
  std::cout << "Push:" << std::endl;
  Push();
  std::cout << "Map:" << std::endl;
  Map();
  std::cout << "Find:" << std::endl;
  Find();
  std::cout << "FindForSorted:" << std::endl;
  FindForSorted();
  std::cout << "FindAll:" << std::endl;
  FindAll();
  std::cout << "Sort:" << std::endl;
  Sort();
  std::cout << "SortBy:" << std::endl;
  SortBy();
  std::cout << "Index:" << std::endl;
  Index();
  std::cout << "All:" << std::endl;
  All();
  std::cout << "Sum:" << std::endl;
  Sum();
  std::cout << "Inject:" << std::endl;
  Inject();
  std::cout << "Uniq:" << std::endl;
  Uniq();
  std::cout << "UniqBy:" << std::endl;
  UniqBy();
  std::cout << "UniqForSorted:" << std::endl;
  UniqForSorted();
  std::cout << "Join:" << std::endl;
  Join();
  std::cout << "Reverse:" << std::endl;
  Reverse();
  std::cout << "GroupBy:" << std::endl;
  GroupBy();
  std::cout << "Shuffle:" << std::endl;
  Shuffle();

  return 0;
}

